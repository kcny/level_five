# Passwords
- Use the PBKDF2 algorithm with an SHA256 hash that is build-in to Django.

```bash
$ pip install bcrypt
```

```bash
$ pip install django[argon2]
```

- SHA -> Secure Hash Algorithm, designed by NSA.
- It's a cryptographic hash functions that can run on digital data.
- [SHA-256 Calculator](https://bit.ly/2viyZgN)

$ python manage.py migrate

$ python manage.py makemigrations basic_app

- Add the code below to settings.py

```python
PASSWORD_HASHERS = [
    'django.contrib.auth.hashers.Argon2PasswordHasher',
    'django.contrib.auth.hashers.BCryptSHA256PasswordHasher',
    'django.contrib.auth.hashers.BCryptPasswordHasher',
    'django.contrib.auth.hashers.PBKDF2PasswordHasher',
    'django.contrib.auth.hashers.PBKDF2SHA1PasswordHasher',
]
```

- The media folder is used for documents that are uploaded by the user etc.

# Users

- Pillow is the python image library and it can be installed usingthe code
  below.

```bash
$ pip install pillow
# If you get an error saying '... jpeg support disabled ...' do this

$ pip install pillow
--global-option="build_text"--global-option="--disable-jpeg"
```

- Use vim 

```vim
:e basic_app/forms.py
```

# User Model Form
- To manipulate data before saving it we set commit=False


# Deployment
[Deploying to Heroku](https://bit.ly/2yTXfdA)
